package com.example.koshik.kidgalleryapp.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import com.example.koshik.kidgalleryapp.R;
import com.squareup.picasso.Picasso;

import roboguice.activity.RoboActionBarActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

@ContentView(R.layout.alphabet_activity_detail)
public class AlphabetDetailActivity extends RoboActionBarActivity {
    public static final String EXTRA_IMAGE = "extra_image";
    @InjectView(R.id.largeImage)
    private ImageView myImage;

    @InjectView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (getIntent() != null && getIntent().getExtras() != null) {
            if (getIntent().getExtras().containsKey(EXTRA_IMAGE)) {
                Picasso.with(this).load(getIntent().getExtras().getString(EXTRA_IMAGE)).into(myImage);
            }
        }
    }
}
