package com.example.koshik.kidgalleryapp.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by koshik on 01-12-2016.
 */

public class AlphabetImage implements Parcelable {

    @SerializedName("thumbnail")
    private String thumbnail;

    @SerializedName("image")
    private String image;

    @SerializedName("caption")
    private String caption;

    public AlphabetImage() {

    }

    public AlphabetImage(String thumbnail, String image, String caption) {
        this.thumbnail = thumbnail;
        this.image = image;
        this.caption = caption;

    }

    protected AlphabetImage(Parcel source) {
        thumbnail = source.readString();
        image = source.readString();
        caption = source.readString();
    }

    public static final Creator<AlphabetImage> CREATOR = new Creator<AlphabetImage>() {
        @Override
        public AlphabetImage createFromParcel(Parcel in) {
            return new AlphabetImage(in);
        }

        @Override
        public AlphabetImage[] newArray(int size) {
            return new AlphabetImage[size];
        }
    };

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        applyDefaultValues();
        parcel.writeString(thumbnail);
        parcel.writeString(image);
        parcel.writeString(caption);
    }

    private void applyDefaultValues() {
        if (thumbnail == null)
            thumbnail = "";
        if (image == null)
            image = "";
        if (caption == null)
            caption = "";
    }
}