package com.example.koshik.kidgalleryapp.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.koshik.kidgalleryapp.R;
import com.example.koshik.kidgalleryapp.models.AnimalImage;
import com.squareup.picasso.Picasso;

import roboguice.activity.RoboActionBarActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;


@ContentView(R.layout.animal_activity_detail)
public class AnimalDetailActivity extends RoboActionBarActivity {
    public static final String EXTRA_ANIMAL = "extra_animal";

    @InjectView(R.id.tvSpecies)
    private TextView species;

    @InjectView(R.id.tvDescription)
    private TextView description;

    @InjectView(R.id.ivImages)
    private ImageView images;

    @InjectView(R.id.toolbar_animal)
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        AnimalImage animalImage = getIntent().getExtras().getParcelable(EXTRA_ANIMAL);

        species.setText(animalImage.getSpecies());
        description.setText(animalImage.getDescription());
        Picasso.with(this).load(animalImage.getImage()).into(images);
    }
}
