package com.example.koshik.kidgalleryapp.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.koshik.kidgalleryapp.R;
import com.example.koshik.kidgalleryapp.models.CountryIModelPojo;
import com.squareup.picasso.Picasso;

import roboguice.activity.RoboActionBarActivity;
import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectExtra;
import roboguice.inject.InjectView;

@ContentView(R.layout.country_flag_detail_activity)

public class CountryFlagDetailActivity extends RoboActionBarActivity {

    @InjectView(R.id.toolbar_robo)
    Toolbar toolbar;

    @InjectView(R.id.tvCountryName)
    private TextView myCountry;

    @InjectView(R.id.tvCountryFullName)
    private TextView myCountryFullName;

    @InjectView(R.id.tvCountryCapitalName)
    private TextView myCapitalName;

    @InjectView(R.id.tvCountryPhoneCode)
    private TextView myCountryPhoneCode;

    @InjectView(R.id.tvCountryLanguage)
    private TextView myCountryLanguage;

    @InjectView(R.id.tvCountryCurrency)
    private TextView myCountryCurrency;

    @InjectView(R.id.tvCountryRegionCode)
    private TextView myCountryRegion;

    @InjectView(R.id.tvCountryDescription)
    private TextView myCountryDescriptions;

    @InjectView(R.id.ivCountryFlagLargeImage)
    private ImageView myImage;
    public static final String EXTRA_COUNTRY_FLAG = "extra_flag";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        CountryIModelPojo modelPojo = getIntent().getParcelableExtra(EXTRA_COUNTRY_FLAG);

        myCountry.setText(modelPojo.getCountryName());
        myCountryFullName.setText(modelPojo.getFullName());
        myCapitalName.setText(modelPojo.getcapitalname());
        myCountryPhoneCode.setText(modelPojo.getPhoneCode());
        myCountryLanguage.setText(modelPojo.getLanguage());
        myCountryCurrency.setText(modelPojo.getCurrency());
        myCountryRegion.setText(modelPojo.getRegion());
        myCountryDescriptions.setText(modelPojo.getDescription());
        Picasso.with(this).load(modelPojo.getImage()).into(myImage);
    }
}